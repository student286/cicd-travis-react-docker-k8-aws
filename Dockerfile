FROM node:16-alpine as build_dir

WORKDIR /app

COPY package.json .

RUN npm install

COPY . .

RUN npm run build

FROM nginx

EXPOSE 80

COPY --from=build_dir /app/build /usr/share/nginx/html



